apt-get install -y jq
apt-get install -y zip

cd layers
export LAYER_VERSION=$(jq -r '.version' package.json) 
echo LAYER_VERSION=$LAYER_VERSION
export package_name=nodejs18
export LAYER_NAME=dev-dmmy
export LAYER_FILE_NAME=$package_name-$LAYER_VERSION.zip
npm i
npm run pack
cd ..

npm i ./layers/layers-$LAYER_VERSION.tgz
mkdir nodejs
mkdir s3
cp -r node_modules nodejs/node_modules
zip -r s3/$LAYER_FILE_NAME nodejs >> /dev/null
rm -rf nodejs
cd lambdas
npm i
npm run build
cd ../cdk
npm i
aws configure list
export STAGE=$1
cdk deploy --all --require-approval never


