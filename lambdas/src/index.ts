import {
  LambdaClient,
  PublishLayerVersionCommand,
  GetFunctionCommand,
  UpdateFunctionConfigurationCommand,
  ListLayerVersionsCommand,
  ListFunctionsCommand,
} from "@aws-sdk/client-lambda";
import { HeadObjectCommand, S3Client } from "@aws-sdk/client-s3";
import { Context, Handler, S3CreateEvent } from "aws-lambda";

const AWS_REGION = process.env.AWS_REGION_NAME;

const lambdaClient = new LambdaClient({
  region: AWS_REGION,
});

const s3Client = new S3Client({
  region: AWS_REGION,
});

const getFunctionsName = async (): Promise<string[]> => {
  const names = process.env.FUNCTIONS_USING_LAYER || "";
  if (names) {
    return names.split(","); // this is for testing, once done we can remove this
  }
  const command = new ListFunctionsCommand({});
  const response = await lambdaClient.send(command);
  return response.Functions?.map((f) => f.FunctionName!!) || [];
};

const getLambdaDetail = async (functionName: string) => {
  const get = new GetFunctionCommand({
    FunctionName: functionName,
  });
  return await lambdaClient.send(get);
};

const getS3ObjectMetadata = async (bucket: string, key: string) => {
  const params = {
    Bucket: bucket,
    Key: key,
  };
  const headObjectCommand = new HeadObjectCommand(params);
  const responseHeadObjct = await s3Client.send(headObjectCommand);
  const metadata = responseHeadObjct.Metadata;
  return metadata;
};

const getLambdaLayerVersions = async (layerName: string) => {
  const command = new ListLayerVersionsCommand({
    LayerName: layerName,
  });
  return (await lambdaClient.send(command)).LayerVersions || [];
};

const getMajorVersionToLatestLayerArnMap = async (
  layerName: string
): Promise<Record<string, string>> => {
  const layerversions = await getLambdaLayerVersions(layerName);
  const map: Record<string, string> = {};
  layerversions.forEach((version) => {
    const majorVersion = version.Description?.split(".")[0];
    if (majorVersion && !map[majorVersion]) {
      map[majorVersion] = version.LayerVersionArn!!;
    }
  });
  return map;
};

const getVersionCodeToSemanticVersionMap = async (
  layerName: string
): Promise<Record<number, string>> => {
  const layerversions = await getLambdaLayerVersions(layerName);
  const map: Record<string, string> = {};
  layerversions.forEach((version) => {
    map[version.Version!!] = version.Description!!;
  });
  return map;
};

const updateLambdasLayerVersion = async (
  layerName: string,
  layerArn: string,
  layerVersionArn: string
) => {
  const functionNames = await getFunctionsName();
  const versionCodeToSemanticVersionMap =
    await getVersionCodeToSemanticVersionMap(layerName);

  const sameMajorVersion = (currentArn: string) => {
    const getVersionCode = (arn: string) => {
      return arn.split(":")[7];
    };

    const getMajorVersion = (code: string) => {
      return versionCodeToSemanticVersionMap[parseInt(code)].split(".")[0];
    };

    return (
      getMajorVersion(getVersionCode(currentArn)) ===
      getMajorVersion(getVersionCode(layerVersionArn))
    );
  };

  const promises = functionNames.map(async (functionName) => {
    try {
      const lambdaDetail = await getLambdaDetail(functionName);
      const configuredLayers = lambdaDetail.Configuration?.Layers;
      if (configuredLayers) {
        var foundNewLayerVersion = false;
        const updatedLayers: string[] = configuredLayers
          .map((layer) => layer.Arn!!)
          .map((arn) => {
            if (arn.startsWith(layerArn) && sameMajorVersion(arn)) {
              foundNewLayerVersion = true;
              return layerVersionArn;
            }
            return arn;
          });

        if (foundNewLayerVersion) {
          const params = {
            FunctionName: functionName,
            Layers: updatedLayers,
          };
          const command = new UpdateFunctionConfigurationCommand(params);
          await lambdaClient.send(command);
          console.log(`Updated layer version for lambda ${functionName}`);
        } else {
          console.log(
            `Lambda(${functionName}) is already using latest major version of layer`
          );
        }
      }
    } catch (error) {
      console.log(error);
    }
  });
  await Promise.all(promises);
};

const publishLayerVersion = async (
  bucket: string,
  key: string,
  version: string,
  layername: string
) => {
  const layerParams = {
    Content: {
      S3Bucket: bucket,
      S3Key: key,
    },
    CompatibleRuntimes: ["nodejs14.x", "nodejs16.x"],
    Description: version,
    LayerName: layername,
  };

  const publishLayerCommand = new PublishLayerVersionCommand(layerParams);

  const response = await lambdaClient.send(publishLayerCommand);
  console.log(`New layer published:\n ${JSON.stringify(response)}`);
  return response;
};

export const handler: Handler = async (
  event: S3CreateEvent,
  context: Context
) => {
  console.log(JSON.stringify(event));

  const record = event.Records[0];
  const bucket = record.s3.bucket.name;
  const key = record.s3.object.key;

  try {
    const metadata = await getS3ObjectMetadata(bucket, key);

    if (!metadata) {
      throw Error("Metadata about the uploaded layer object not found");
    }

    const { layername, version } = metadata;

    const response = await publishLayerVersion(bucket, key, version, layername);

    await updateLambdasLayerVersion(
      layername,
      response.LayerArn!!,
      response.LayerVersionArn!!
    );
  } catch (error) {
    console.error(error);
  }
};
