import {
  Context,
  Handler,
  S3CreateEvent,
} from "aws-lambda";

export const handler: Handler = async (
  event: S3CreateEvent,
  context: Context
) => {
  console.log(JSON.stringify(event));
};
