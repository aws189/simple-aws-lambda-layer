#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { CdkStack } from '../lib/cdk-stack';
import { Stage } from '../lib/constant';

const getStageToDeploy = ()=>{
  const stageArg = process.env.STAGE;
  if(Stage.PROD == stageArg){
    return Stage.PROD;
  }
  return Stage.DEV;
}

const stage = getStageToDeploy();

const app = new cdk.App();
new CdkStack(app, `CdkStack-${stage}`, {
  env: {
    region: "ap-south-1",
    account: "323439077171",
  },
  stage: stage,
});
