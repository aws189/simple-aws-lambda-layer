import * as cdk from "aws-cdk-lib";
import * as lambda from "aws-cdk-lib/aws-lambda";
import * as s3 from "aws-cdk-lib/aws-s3";
import * as iam from "aws-cdk-lib/aws-iam";
import * as path from "path";
import { Construct } from "constructs";
import { Stage } from "./constant";
import * as lambdaEventSources from "aws-cdk-lib/aws-lambda-event-sources";
import * as s3deploy from "aws-cdk-lib/aws-s3-deployment";

interface CdkStackProps extends cdk.StackProps {
  stage: Stage;
}

export class CdkStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props: CdkStackProps) {
    super(scope, id, props);
    const { stage } = props;
    const prefix = `${stage}`;

    const artifacBucket = new s3.Bucket(this, `${prefix}-layers-zip-artifact`, {
      bucketName: `${prefix}-layers-zip-artifact`,
    });

    artifacBucket.applyRemovalPolicy(cdk.RemovalPolicy.DESTROY);

    const lambdaRole = new iam.Role(this, `${prefix}-LambdaRole`, {
      assumedBy: new iam.ServicePrincipal("lambda.amazonaws.com"),
    });

    lambdaRole.addToPolicy(
      new iam.PolicyStatement({
        actions: [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
        ],
        resources: ["*"],
      })
    );

    lambdaRole.addToPolicy(
      new iam.PolicyStatement({
        actions: [
          "lambda:PublishLayerVersion",
          "lambda:ListFunctions",
          "lambda:GetFunction",
          "lambda:UpdateFunctionConfiguration",
          "lambda:ListLayerVersions",
          "lambda:GetLayerVersion",
        ],
        resources: ["*"],
        effect: cdk.aws_iam.Effect.ALLOW,
      })
    );
    artifacBucket.grantRead(lambdaRole);

    // for tsting use only
    const getFunctionsName = () => {
      if (stage === Stage.DEV) {
        return this.createTestLambda(prefix).functionName;
      } else {
        return "";
      }
    };

    const lambdaFunction = new lambda.Function(
      this,
      `${prefix}-process-new-layer-upload`,
      {
        runtime: lambda.Runtime.NODEJS_18_X,
        functionName: `${prefix}-process-new-layer-upload`,
        handler: "index.handler",
        code: lambda.Code.fromAsset(path.join("../lambdas/dist")),
        role: lambdaRole,
        environment: {
          LAYERS_ZIP_BUCKET_NAME: artifacBucket.bucketName,
          FUNCTIONS_USING_LAYER: getFunctionsName(),
          AWS_REGION_NAME: "ap-south-1",
        },
        timeout: cdk.Duration.minutes(10),
      }
    );

    lambdaFunction.addEventSource(
      new lambdaEventSources.S3EventSource(artifacBucket, {
        events: [s3.EventType.OBJECT_CREATED],
      })
    );

    const uploads = new s3deploy.BucketDeployment(this, "DeployFiles", {
      sources: [s3deploy.Source.asset(`../s3`)],
      destinationBucket: artifacBucket,
      metadata: {
        version: `${process.env.LAYER_VERSION}`,
        layername: `${process.env.LAYER_NAME}`,
      },
    });
  }

  createTestLambda(prefix: string): lambda.Function {
    const lambdaRole = new iam.Role(this, `${prefix}-test-layer-lambda-role`, {
      assumedBy: new iam.ServicePrincipal("lambda.amazonaws.com"),
    });

    lambdaRole.addToPolicy(
      new iam.PolicyStatement({
        actions: [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
        ],
        resources: ["*"],
      })
    );

    const lambdaFunction = new lambda.Function(
      this,
      `${prefix}-test-layer-lambda`,
      {
        runtime: lambda.Runtime.NODEJS_16_X,
        functionName: `${prefix}-test-layer-lambda`,
        handler: "test.handler",
        code: lambda.Code.fromAsset(path.join("../lambdas/dist")),
        role: lambdaRole,
        environment: {},
        timeout: cdk.Duration.minutes(10),
        layers: [],
      }
    );
    return lambdaFunction;
  }
}
