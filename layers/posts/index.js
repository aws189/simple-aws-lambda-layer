import axios from "axios";

let config = {
  method: "get",
  maxBodyLength: Infinity,
  url: "https://jsonplaceholder.typicode.com/posts",
  headers: {},
};

export const getPosts = async () => {
  try {
    const res = await axios.request(config);
    return res.data;
  } catch (err) {
    throw err;
  }
};
