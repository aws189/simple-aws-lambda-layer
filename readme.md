# AWS Lambda layer for nodejs 18

## Steps to create aws lambda layer

1. Create a folder `layers`
2. Now, Init a npm project inside `layers` folder
3. Update `type: 'module'` in `package.json`
   ```json
   {
     "name": "layers",
     "version": "1.0.0",
     "description": "",
     "main": "index.js",
     "type": "module",
     "scripts": {
       "pack": "npm pack"
     },
     "keywords": [],
     "author": "",
     "license": "ISC",
     "dependencies": {
       "axios": "^1.4.0"
     }
   }
   ```
4. Write your shared bussiness logic
   ```js
   import axios from "axios";
   export const getPosts = async () => {
     return [
       {
         id: 1,
         title: "Demo",
         content: "This can be some more meaning then this.",
       },
     ];
   };
   ```
5. We can re-export different module from root, for that create `index.js` and export module you want
   ```js
   export * from "./posts";
   ```
6. Now yo need to run this command in layers dir `npm run pack`
7. After this come to the root folder and run `npm init` we use this to create `node_moduldes` folder for creating layer
8. Copy the `node_modules` folder in `nodejs` and create a zip.
9. Create aws layer using this zip via CLI or aws console

## How to use the above layer in another lambda

1. Add layer to your lambda runtime
2. Import module you want to use in your lambda code, like below sample code

   ```js
   import { getPosts } from "layers";

   export const handler = async (event) => {
     // TODO implement
     const response = {
       statusCode: 200,
       body: JSON.stringify(await getPosts()),
     };
     return response;
   };
   ```
